import io
import numpy as np
from flask import Flask, request, Response, send_file
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
filePath="/home/luca/PycharmProjects/FlaskExercise/venv/bin/"

def create_figure(max_x):
	x = np.arange(0.0, max_x + 0.01, 0.01)
	y = np.sin(x)
	fig = Figure()
	plt = fig.add_subplot(1, 1, 1)
	plt.plot(x, y)
	return fig

form = '''
<html>
<body>
Stampo il grafico di sin(x)
<form action="/plot">
  Numero di periodi nel plot:<br>
  <input type="text" name="periodi"><br>
  <input type="submit" value="Submit">
</form>
</body>
</html>
'''

app = Flask (__name__)

@app.route("/")
def index():
    return form



@app.route("/plot")
def plot_png():
	periods = float(request.args.get('periodi'))
	max_x = 2*np.pi*periods
	fig = create_figure(max_x)
	output = io.BytesIO()
	FigureCanvas(fig).print_png(output)
	return Response(output.getvalue(), mimetype='image/png')

def controllaVoto(studente):
	if int(studente[2])>=18 and int(studente[2])<=30:
		return 1
	else:
		return -1
import json
import csv
@app.route("/voti", methods=['POST'])
def elabora_voti():
	if request.method=="POST":
		f = request.files['file']
		f.save("voti_elab.csv")
		votiValidi=[]
		with open('voti_elab.csv') as csv_file:
			csv_reader = csv.reader(csv_file, delimiter=',')
			for row in csv_reader:
				if controllaVoto(row) == 1 :
					votiValidi.append(row)
		data=[]
		for studente in votiValidi:
			elem={}
			elem["nome"]=studente[0]
			elem["cognome"] = studente[1]
			elem["voto"] = studente[2]
			data.append(elem)
		#Crea un oggetto JSON
		#json_data = json.dumps(data)

		with open('json.txt', 'w') as outfile:
			json.dump(data, outfile)
	return "ok"


@app.route("/getVoti", methods=['GET'])
def getVoti():
	with open('json.txt') as json_file:
		data = json.load(json_file)
		print(data)
	f = open("json.txt", "rb")
	return send_file(filePath+"json.txt", as_attachment=True)



##if __name__ == "__main__":
  #  app.run()
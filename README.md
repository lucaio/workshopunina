# workshopunina

# Es1:
Si crei un programma client in grado di creare un file CSV contenente per ogni riga le informazioni sull'esame di uno studente 
-Nome Studente
-Cognome Studente
-Voto Esame 

Tale file csv dovrà esser successivamente inviato a un server web (usando un microframework come Flask o simili ) . Tale file dovrà essere quindi essere
analizzato e per ogni studente si dovrà verificare che i voti inseriti siano nel range 18-30. 
Usando solo le informazioni degli studenti con un voto che rientra nei range si dovrà creare un file json e renderlo disponibile per
il download su una certa pagina web 

Step: 
- Client: Scrittura su file 
- Client: richiesta Post al server http://docs.python-requests.org/en/master/user/quickstart/ 
- Server: Creazione Handler con recupero file
- Server: creazione Json 


# Es2: 
Si crei una funzione in grado di plottare una funzione seno per un numero di periodi specificati dall'utente. Tale funzione dovrà essere 
implementata su un webserver (usando un microframework come Flask o simili ) e ricevere il numero di periodi tramite un form html 

- Generazione di un grafico della funzione seno
- Creazione form html
- Handler per mostrare il grafico

#Installazione Dipendenze 
- pythonX -m pip install flask
- pythonX -m pip install matlibplot : https://matplotlib.org/tutorials/index.html#introductory
- pythonX -m pip install numpy 
- X=version 

Configurazione: 
- http://flask.pocoo.org/docs/1.0/quickstart/
- Settare variabile d'ambiente 
FLASK_APP=nomeFile.py
- Nell'IDE come script da eseguire inserire il path dell'eseguibile di flask
- Come argomento da passare usare "run"

